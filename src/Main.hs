{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Main (
      main
    ) where

import Control.Lens(view)
import Control.Monad (when, unless)
import Data.Complex(realPart)
import qualified Data.Map.Strict as Map
import qualified Data.Mutable as Mut
import Data.Vector.Storable.Mutable (IOVector)
import qualified Data.Vector.Storable.Mutable as Vector
import Data.Word(Word8)
import Data.Text (Text, pack)
import qualified Data.WAVE as WAV
import Foreign.C.Types (CInt)
import Foreign.Ptr(castPtr, plusPtr)
import Foreign.Storable(poke)
import Linear(V4(V4), V2(V2), _x, _y)
import qualified SDL
import qualified SDL.Audio as Au
import SDL.Audio(Changeable(Desire))
import qualified SDL.Font as Font
import System.Environment (getArgs)
import System.Exit (exitWith, ExitCode(ExitFailure))
import Tafa.Types(Time(Time), Frequency(Frequency), Response(Response))
import Tafa.Wavelet(cwt, morlet')

import Debug.Trace(trace)

white = V4 255 255 255 255
black = V4 255 0 0 0
bufferSize = 4096

main :: IO ()
main = do
    args <- getArgs
    when (length args == 0) $ putStrLn "Specify filename to play" >> exitWith (ExitFailure 1)
    let audioFilename = head args

    -- todo: consider reading other formats
    wave <- WAV.getWAVEFile audioFilename
    let waveFrameRate = (WAV.waveFrameRate . WAV.waveHeader) wave
    mWaveSamples <- fmap Mut.asIORef $ Mut.newRef (zip [Time 0.0, Time (1.0/fromIntegral waveFrameRate) .. ] $ WAV.waveSamples wave)
    mSignalMap <- fmap Mut.asIORef $ Mut.newRef Map.empty
    mTfrCache <- fmap Mut.asIORef $ Mut.newRef Map.empty
    mCurrentTime <- fmap Mut.asIORef $ Mut.newRef (Time 0)
    
    SDL.initializeAll
    Font.initialize
    regularFont <- Font.load "font/regular.ttf" 12 
    boldFont <- Font.load "font/bold.ttf" 12

    window <- SDL.createWindow "Time and Frequency Audio GUI" $ SDL.defaultWindow {SDL.windowResizable = True}
    renderer <- SDL.createRenderer window 0 SDL.defaultRenderer
    

    let callback :: Au.AudioFormat s -> IOVector s -> IO ()
        callback format mvec = do
            case format of
                Au.FloatingLEAudio -> Vector.set mvec 0 
                _ -> return ()

            let copySample index = do
                    frame <- Mut.popFront mWaveSamples
                    case frame of
                        Just (time, f) -> do
                            let sample = head f -- note that this only grabs the first channel. Stereo is ignored.
                                realSample = WAV.sampleToDouble sample
                            Mut.writeRef mCurrentTime time
                            Mut.modifyRef mSignalMap $ Map.insert time realSample
                            case format of
                                Au.FloatingLEAudio -> Vector.write mvec (fromIntegral index) (realToFrac realSample) 
                                _ -> return () -- todo: support other formats
                            unless (index + 1 >= bufferSize) $ copySample (index + 1)
                        Nothing -> return ()
            copySample 0

        openDeviceSpec = Au.OpenDeviceSpec 
            (Desire $ fromIntegral waveFrameRate) -- output audio frequency
            (Desire Au.FloatingNativeAudio) -- audio format
            (Desire Au.Mono) -- channels
            bufferSize
            (callback)
            (Au.ForPlayback)
            Nothing -- name of the device
    (auDevice, auSpec) <- Au.openAudioDevice openDeviceSpec
    auDriver <- Au.currentAudioDriver
    Au.setAudioDevicePlaybackState auDevice Au.Play
    
    let
        createTexture :: (V2 CInt) -> IO (SDL.Texture)
        createTexture size = SDL.createTexture renderer SDL.RGBA8888 SDL.TextureAccessStreaming size
        createOverlayTexture :: (V2 CInt) -> IO (SDL.Texture)
        createOverlayTexture size = do
            intermediateSurface <- SDL.createRGBSurface size SDL.RGBA8888
            blitText boldFont (V2 10 10) intermediateSurface $
                case auDriver of
                    Just name -> name
                    Nothing -> "No Audio Driver Loaded!"
            blitText regularFont (V2 10 30) intermediateSurface $ pack $ show (Au.audioSpecFreq auSpec) ++ " Hz"
            blitText regularFont (V2 10 45) intermediateSurface $ pack $ show (Au.audioSpecChannels auSpec)
            blitText regularFont (V2 10 60) intermediateSurface $ pack $ "Max buffer size: " ++ show (Au.audioSpecSize auSpec) ++ "B"
            tex <- SDL.createTextureFromSurface renderer intermediateSurface
            SDL.freeSurface intermediateSurface
            return tex
        
    mTfrTexture <- (fmap Mut.asIORef . Mut.newRef) =<< createTexture (V2 1 1)
    mOverlayTexture <- (fmap Mut.asIORef . Mut.newRef) =<< createOverlayTexture (V2 1 1)

    let mainLoop :: IO ()
        mainLoop = do
            winSize <- SDL.get $ SDL.windowSize window
            texSize <- textureSize =<< Mut.readRef mTfrTexture
            unless (winSize == texSize) $ do
                SDL.destroyTexture =<< Mut.readRef mTfrTexture
                SDL.destroyTexture =<< Mut.readRef mOverlayTexture
                Mut.writeRef mTfrTexture =<< createTexture winSize
                Mut.writeRef mOverlayTexture =<< createOverlayTexture winSize
            tfrTexture <- Mut.readRef mTfrTexture
            overlayTexture <- Mut.readRef mOverlayTexture
            
            let coloredTfr :: (RealFloat a) => Map.Map (Time a) a -> (Time a, Frequency a) -> V4 Word8
                coloredTfr sigMap = toColor . cwt morlet' sigMap
                
                preloadSpectrogram :: Time Double -> [Frequency Double] -> IO ()
                preloadSpectrogram time freqs = do
                    sigMap <- Mut.readRef mSignalMap
                    let narrowedSigMap = between (time - Time 0.05, time + Time 0.05) sigMap
                        newMapKeys = (\f -> (time, f)) <$> freqs
                        newMapValues = Map.fromDistinctAscList $ (\tf -> (tf, coloredTfr sigMap tf)) <$> newMapKeys
                    Mut.modifyRef mTfrCache $ Map.union newMapValues
                    
                updatedSpectrogram :: [Time Double] -> [Frequency Double] -> IO ((Time Double, Frequency Double) -> V4 Word8)
                updatedSpectrogram times freqs = do
                    tfrCache <- Mut.readRef mTfrCache
                    
                    let cachedUntil = case Map.lookupMax tfrCache of
                                            Nothing -> Time 0
                                            Just ((t, _),_) -> t
                    putStrLn $ "cached until " ++ show cachedUntil
                    
                    sequence $ (\t' -> preloadSpectrogram t' freqs) <$> dropWhile (<= cachedUntil) times
                    updTfrCache <- Mut.readRef mTfrCache
                    
                    return $ \tf ->
                        Map.findWithDefault black tf updTfrCache
                        
            

            let frequencies = Frequency <$> logdist 20 (realToFrac waveFrameRate/2) (realToFrac $ view _y winSize) 
                times = Time <$> lindist 0 10 (realToFrac $ view _x winSize)
                
            spectrogram <- updatedSpectrogram times frequencies
            let coords = cartesian times frequencies
                coordsLen = fromIntegral $ (view _x winSize) * (view _y winSize)
                colorData :: [Word8]
                colorData = foldr (++) [] $ take coordsLen $ (toList4 <$> (spectrogram <$> coords))
            
            (ptr, pitch) <- SDL.lockTexture tfrTexture Nothing
            let pokeOver _ [] = return ()
                pokeOver ptr' (w:ws) = do
                    poke ptr' w
                    pokeOver (plusPtr ptr' 1) ws
            pokeOver (castPtr ptr) colorData
            SDL.unlockTexture tfrTexture
            SDL.copy renderer tfrTexture Nothing Nothing
            SDL.copy renderer overlayTexture Nothing Nothing
            SDL.present renderer
            SDL.delay(30000)

            events <- SDL.pollEvents
            unless (any (eventIsPress SDL.KeycodeQ) events) $ mainLoop


    mainLoop
    
    SDL.destroyTexture =<< Mut.readRef mTfrTexture
    SDL.destroyTexture =<< Mut.readRef mOverlayTexture
    Font.free regularFont
    Font.free boldFont
    Font.quit
    SDL.quit
    
logdist :: (Enum a, Floating a) => a -> a -> a -> [a]
logdist start stop count = exp <$> lindist (log start) (log stop) count

lindist :: (Enum a, Fractional a) => a -> a -> a -> [a]
lindist start stop count =
    let step = (stop-start)/(count - 1)
    in [start, start + step .. stop]

blitText :: Font.Font -> V2 CInt -> SDL.Surface -> Text -> IO ()
blitText font pos surface text = do
    textSurface <- Font.solid font white text
    SDL.surfaceBlit textSurface Nothing surface (Just $ SDL.P $ pos) 
    SDL.freeSurface textSurface
    return ()
        
eventIsPress :: SDL.Keycode -> SDL.Event -> Bool
eventIsPress keycode event = 
    case SDL.eventPayload event of
        SDL.KeyboardEvent keyboardEvent ->
            SDL.keyboardEventKeyMotion keyboardEvent == SDL.Pressed &&
            SDL.keysymKeycode (SDL.keyboardEventKeysym keyboardEvent) == keycode
        _ -> False
        
textureSize :: SDL.Texture -> IO (V2 CInt)
textureSize texture = do
    info <- SDL.queryTexture texture
    return $ V2 (SDL.textureWidth info) (SDL.textureHeight info)

cartesian :: [x] -> [y] -> [(x,y)]
cartesian xs ys = [(x,y) | y <- ys, x <- xs]

toList4 :: V4 a -> [a]
toList4 (V4 a b c d) = [a,b,c,d]

-- NOTE: the color here is in the format (A,B,G,R)
toColor :: (RealFloat a) => Response a -> V4 Word8
toColor (Response c)
    | x <= 0 = black
    | x >= 1 = white
    | otherwise = V4 g g g g 
  where
    g = fromIntegral $ round (255 * x)
    x = realPart $ abs c
    
    
between :: (Ord k) => (k, k) -> Map.Map k v -> Map.Map k v
between (start, end) = fst . Map.split end . snd . Map.split start

